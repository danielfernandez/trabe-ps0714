package com.trabesoluciones.springtest.model.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.trabesoluciones.springtest.model.pojos.Car;
import com.trabesoluciones.springtest.model.pojos.Category;
import com.trabesoluciones.springtest.model.pojos.accessors.CarAccessor;

@Service
public class CarServiceImpl implements CarService {

    @Autowired
    private CarAccessor carAccessor;

    @Override
    public List<Car> findAll() {
        return carAccessor.findAll();
    }

    @Override
    public List<Car> findByCategoryId(int categoryId) {
        return carAccessor.findByCategory(categoryId);
    }

    @Override
    public List<Category> findCategories() {
        return carAccessor.findCategories();
    }

}
