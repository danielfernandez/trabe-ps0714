package com.trabesoluciones.springtest.model.services;

import java.util.List;

import com.trabesoluciones.springtest.model.pojos.Car;
import com.trabesoluciones.springtest.model.pojos.Category;

public interface CarService {

    public List<Car> findAll();

    public List<Car> findByCategoryId(int categoryId);

    public List<Category> findCategories();
}
