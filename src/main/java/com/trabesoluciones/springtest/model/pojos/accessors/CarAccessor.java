package com.trabesoluciones.springtest.model.pojos.accessors;

import java.util.List;

import com.trabesoluciones.springtest.model.pojos.Car;
import com.trabesoluciones.springtest.model.pojos.Category;

public interface CarAccessor {
    public List<Car> findAll();

    public List<Car> findByCategory(int categoryId);

    public List<Category> findCategories();
}
