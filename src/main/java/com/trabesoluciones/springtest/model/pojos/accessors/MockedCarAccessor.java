package com.trabesoluciones.springtest.model.pojos.accessors;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.trabesoluciones.springtest.model.pojos.Car;
import com.trabesoluciones.springtest.model.pojos.Category;

@Component
public class MockedCarAccessor implements CarAccessor {

    private static List<Category> categories;

    private static List<Car> cars = new ArrayList<>();

    private static Map<Integer, List<Car>> carsByCategory = new HashMap<>();

    static {
        categories = Arrays.asList(
            new Category(1, "Sports Cars"),
            new Category(2, "Small Cars"),
            new Category(3, "Family Cars"));

        List<Car> sportsCars = Arrays.asList(
            new Car(1, "Carrera", "Porsche", 350, 1),
            new Car(2, "F40", "Ferrari", 400, 1),
            new Car(3, "Pantera", "DeTomaso", 300, 1));

        List<Car> smallCars = Arrays.asList(
            new Car(4, "Punto", "Fiat", 60, 2),
            new Car(5, "Twingo", "Renault", 50, 2),
            new Car(6, "Arousa", "Seat", 40, 2));

        List<Car> familyCars = Arrays.asList(
            new Car(7, "5 Series", "BMW", 240, 3),
            new Car(8, "Exeo", "Seat", 150, 3),
            new Car(9, "A6", "Audi", 200, 3));

        carsByCategory.put(1, sportsCars);
        carsByCategory.put(2, smallCars);
        carsByCategory.put(3, familyCars);

        cars.addAll(sportsCars);
        cars.addAll(smallCars);
        cars.addAll(familyCars);
    }

    @Override
    public List<Car> findAll() {
        return cars;
    }

    @Override
    public List<Category> findCategories() {
        return categories;
    }

    @Override
    public List<Car> findByCategory(int categoryId) {
        return carsByCategory.get(categoryId);
    }

}
