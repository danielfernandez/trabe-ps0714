package com.trabesoluciones.springtest.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.trabesoluciones.springtest.model.pojos.Car;
import com.trabesoluciones.springtest.model.pojos.Category;
import com.trabesoluciones.springtest.model.services.CarService;

@Controller
@RequestMapping("/api/internal")
public class InternalApiController {

    @Autowired
    private CarService carService;

    @RequestMapping(value = "/cars", method = RequestMethod.GET)
    public @ResponseBody
    List<Car> findAllCars() {
        return carService.findAll();
    }

    @RequestMapping(value = "/categories", method = RequestMethod.GET)
    public @ResponseBody
    List<Category> findAllCategories() {
        return carService.findCategories();
    }

    @RequestMapping(value = "/categories/{categoryId}/cars", method = RequestMethod.GET)
    public @ResponseBody
    List<Car> findCarsByCategory(@PathVariable Integer categoryId) {
        return carService.findByCategoryId(categoryId);
    }

}
