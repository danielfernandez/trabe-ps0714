# Ejercicio práctico

## Bottom line

Se trata de hacer unos pequeños cambios en una mini aplicación spring mvc  

## Contexto

Hemos creado una pequeñísima aplicación web spring MVC. Para la vista hemos utilizado thymeleaf 
(http://www.thymeleaf.org/doc/usingthymeleaf.html). Asumiendo que tienes java 7 y maven 
instalados en tu equipo, puedes lanzar la aplicación con el siguiente comando:

        mvn clean tomcat7:run-war -e

Si todo va bien, tendrás la aplicación disponible en el puerto 8080 de tu máquina, en esta url: 
		
		http://localhost:8080/test

En la aplicación hemos definido un layout muy básico (base-layout.html) y hemos creado una página 
de inicio (home.html) que será donde mostrarás el resultado de tu ejercicio.

En este ejercicio vamos a trabajar con coches, organizados en categorías.Hemos implementado un 
API (InternalApiController.java) que expone tres operaciones:

* http://localhost:8080/test/categories: proporciona un json con las categorías de coches que hay en
el sistema.
* http://localhost:8080/test/cars: proporciona un json con todos los coches que hay en 
el sistema.
* http://localhost:8080/test/categories/{category_id}/cars: proporciona un json con todos los coches
de la categoría {categoryId}

## Tareas

1. Conseguir que el API devuelva los coches ordenados por potencia. Al igual que en el test teórico 
que ya has hecho, asumimos que el orden natural de los coches es por potencia y tendremos esta 
información en cuenta para implementar esta feature.
2. Queremos que saques en la página home un listado de coches ordenados por potencia. La idea es que 
en la plantilla html pintes los enlaces para navegar entre categorías. El listado en si mismo, 
así como el repintado de la página cuando selecciones una nueva categoría debería hacerse obteniendo 
los datos del API de forma asíncrona (javascript).

## Cosas opcionales

1. Opcionalmente, si lo deseas, puedes cambiar la implementación "mockeada" de Car accesor por una 
implementación que lea los datos de uno o varios ficheros json (por ejemplo utilizando gson)
2. Opcionalmente, si lo deseas, puedes añadir algún test que consideres oportuno.
